Context Menu Search
===================
크롬의 컨텍스트 메뉴에 ... 에서 검색하기 메뉴를 추가한다.

기본으로 네이버를 추가한다.

Development
-----------
### Prerequisites
- NodeJS
- Gulp (`npm install -g gulp-cli`)

### Build
Gulp를 이용해서 빌드한다.

#### Tasks
- local
  로컬에서 개발자 버전으로 크롬 확장팩에 설치할 수 있도록 `dist/source` 폴더에 복사한다. 
- zip
  Chrome Web Store에 배포할 수 있도록 압축 파일을 생성한다.
