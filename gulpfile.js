const fs = require('fs')
const gulp = require('gulp')
const clean = require('gulp-clean')
const jeditor = require('gulp-json-editor')
const zip = require('gulp-zip')


gulp.task('clean', () =>
  gulp.src('dist', {read: false})
    .pipe(clean())
)

gulp.task('copy', () =>
  gulp.src('src/*')
    .pipe(gulp.dest('dist/src'))
)

gulp.task('local', ['copy'], () => {
  const version = JSON.parse(fs.readFileSync('./package.json')).version
  return gulp.src('dist/src/manifest.json')
    .pipe(jeditor({
      'version': version
    }))
    .pipe(gulp.dest('dist/src/'))
})

gulp.task('zip', ['local'], () =>
  gulp.src('dist/src/*')
    .pipe(zip('archive.zip'))
    .pipe(gulp.dest('dist'))
)

gulp.task('default', ['zip'])

