var engine = {};
var engineName, url;

function saveEngine() {
	try {
		engine["name"] = engineName.value;
		engine["url"] = url.value;
		localStorage["rightSearch"] = JSON.stringify(engine);

		chrome.contextMenus.update("cmSearch", {
			"title":engine["name"] 
		});
	} catch(e) {
		console.log(e);
	}
}

function loadEngine() {
	try {
		engine = JSON.parse(localStorage["rightSearch"]);
		engineName.value = engine["name"];
		url.value = engine["url"];

	} catch(e) {
		console.log(e);
	}
}

function init() {
	engineName = document.getElementById("engineName");
	url = document.getElementById("url");

	loadEngine();

	var save = document.getElementById("save");
	var cancel = document.getElementById("cancel");

	save.addEventListener("click", function (e) {
		saveEngine();
	});

	cancel.addEventListener("click", function (e) {
		loadEngine();
	});
}

document.addEventListener('DOMContentLoaded', init());
