function onClickHandler(info, tab) {
	var tabId = tab.id;
	var engine = JSON.parse(localStorage["rightSearch"]);
	var url = engine["url"] || "http://search.naver.com/search.naver?ie=utf8&query=%s";
	
	chrome.tabs.sendMessage(tabId, {}, function(response) {
		url = url.replace("%s", response);
		chrome.tabs.create({"url":url});
	});

}

function addContextMenu() {
	var engine = JSON.parse(localStorage["rightSearch"]);
	var engineName = engine["name"] || "네이버에서 검색하기";

	chrome.contextMenus.create({
		"title":engineName, "contexts":["selection"], "id":"cmSearch"
	});

}
chrome.contextMenus.onClicked.addListener(onClickHandler);

// 확장프로그램이 처음 설치되었을 때 실행된다.
chrome.runtime.onInstalled.addListener(function() {
	localStorage["rightSearch"] = '{"name":"네이버에서 검색하기","url":"http://search.naver.com/search.naver?ie=utf8&query=%s"}';
	addContextMenu();
});

// 크롬이 처음 시작되었을 때 실행된다.
chrome.runtime.onStartup.addListener(function() {
	addContextMenu();
});
