function select() {
	var selection = window.getSelection().toString();

	if (selection == '') {
		for(var i = 0; i < window.frames.length; i++) {
			selection = window.frames[i].document.getSelection().toString();
			if (selection != '')
				break;
		}
	}

	return selection;
}

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
	sendResponse(select());
});
